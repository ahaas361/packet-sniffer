import re, sys
from PacketSniffer import *

# Help descriptions
template = "\n{}\n\n{}\n"
helpDescr = template.format("help [command]","Displays help information for the given command.\nIf no command is given it displays the list of commands.")
exitDescr = template.format("exit","Exits the program.")
captureDescr = template.format("capture duration","Captures all outgoing and incoming network traffic for the given duration of time in seconds.\nCaptured packets are dumped into 'dump.txt'.")
searchDescr = template.format("search regex","Searches and returns all captured HTTP packets that contain the given regex.")
httpDescr = template.format("http (list|reconstruct id)","list: Lists all captured HTTP packets.\nreconstruct: Reconstructs the full HTML page for the given HTTP packet.")
dnsDescr = template.format("dns (list|reconstruct id)","list: Lists all captured DNS packets.\nreconstruct: Reconstructs the DNS query and response for the given DNS packet.")
descriptions = {"help":helpDescr, "exit":exitDescr, "capture":captureDescr, "search":searchDescr, "http":httpDescr, "dns":dnsDescr}

def helpAction(args):
	global descriptions
	length = len(args)
	if length == 0:
		helpMenu()
	elif length==1:
		command = args[0]
		if validCommand(command):
			print(descriptions[command])
		else:
			invalid(1)
	elif length>1:
		invalid(2)

def exitAction(args):
	if len(args)>0:
		invalid(2)
		return
	print("Goodbye.")
	sys.exit()

def captureAction(args):
	length = len(args)
	if length==0:
		invalid(4)
	elif length>1:
		invalid(2)
	else:
		duration = args[0]
		print("Capturing packets for "+duration+" seconds...")
		capture(int(duration))
		print("Capture complete. The dump is located in 'dump.txt' and the raw data is located in 'RawData.txt'.")

def searchAction(args):
	length = len(args)
	if length==0:
		invalid(2)
	elif length>1:
		if args[0].startswith('"') or args[0].startswith("'"):
			quote = args[0][0]
			regex = " ".join(args)
			if not regex.endswith(quote):
				invalid(2)
			else:
				regex = regex.strip(quote)
				print("Searching packets for the regex '"+regex+"'...")
				search(regex)
				print("Search complete.")

		else:
			invalid(2)
	else:
		if args[0].startswith('"') or args[0].startswith("'"):
			quote = args[0][0]
			if args[0].endswith(quote):
				regex = args[0].strip(quote)
		else:
			regex = args[0]
		print("Searching packets for the regex '"+regex+"'...")
		search(regex)
		print("Search complete.")

def httpAction(args):
	length = len(args)
	if length==0:
		invalid(4)
	elif length>2:
		invalid(2)
	elif args[0]=="list":
		if length>1:
			invalid(2)
		else:
			print("Listing all HTTP packets...")
			httpList()
	elif args[0]=="reconstruct":
		if length==1:
			invalid(4)
		else:
			print("Reconstructing the HTTP for packet "+args[1]+"...")
			httpReconstruct(args[1])
	else:
		invalid(3)

def dnsAction(args):
	length = len(args)
	if length==0:
		invalid(4)
	elif length>2:
		invalid(2)
	elif args[0]=="list":
		if length>1:
			invalid(2)
		else:
			print("Listing all DNS packets...")
			dnsListAction()
	elif args[0]=="reconstruct":
		if length==1:
			invalid(4)
		else:
			print("Recontructing the DNS query and response for packet "+args[1]+"...")
			dnsRecAction(args[1])
	else:
		invalid(3)

def dnsListAction():
	print("DNS list")

def dnsRecAction(id):
	print("DNS reconstruct")

# List of accepted commands and associated methods
commands = {"help":helpAction, "exit":exitAction, "capture":captureAction, 
		"search":searchAction, "http":httpAction, "dns":dnsAction}

#Process user input
def process(user_input):
	global commands
	user_input = re.sub(r'[<>,\s]',' ',user_input)
	user_input = user_input.split()
	if user_input == []:
		return (False, None, None)
	else:
		command = user_input[0]
		args = user_input[1:]
		if command not in commands:
			return (False, command, args)
		return (True,command, args)

# Checks whether a given command is valid
def validCommand(c):
	global commands
	return (c in commands)

# Prints error messages for various cases
def invalid(n):
	useHelp = "Use 'help [command]' to learn how to use a command."
	if n==1:
		print("You have input an incorrect command. Type 'help' for a list of valid commands.")
	elif n==2:
		print("Too many arguments have been supplied.",useHelp)
	elif n==3:
		print("That is not an action associated with this command.",useHelp)
	elif n==4:
		print("Not enough arguments have been supplied for this command.",useHelp)
	else:
		print("Something went wrong.")

# Prints out commands
def helpMenu():
	print("The available commands are:")
	c = [x for x in commands.keys()]
	c.sort()
	print("   ".join(c))
	print('\nType "help [command]" for more information on a specific command')

def main():
	# Prints out the help menu
	helpMenu()
	# Asks for user input, performs input, repeats
	while(True):
		user_input = input("> ")
		valid, command, args = process(user_input)
		if valid==False and command is not None:
			invalid(1)
			continue
		elif valid==False:
			continue
		commands[command](args)

main()
