import socket, sys, binascii, time, csv, re
from struct import *

dumpFile = "dump.txt"
rawData = "RawData.txt"
delimiter = "||||"
httpCount = 1

# duration of time to carpture packets, in seconds
def capture(duration):
	rawSocket = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.htons(0x0003))
	# duration = 5
	timeout = time.time() + duration
	allPackets = []
	rawFile = open(rawData, 'w')
	while timeout>time.time():
		#packet size == 65535
		packet, address = rawSocket.recvfrom(65535)
		rawFile.write(packet.decode("utf-8", "ignore"))
		rawFile.write("\n")
		#first 14 bytes of ethernet frame header give source mac, dest mac, and protocol
		packetString = ""
		ethernetHeaderLength = 14

		#gets first 14 bytes
		ethernetHeader = packet[:ethernetHeaderLength]

		#ethFrame is now a tuple that contains the dest mac, source mac, and the protocol  
		ethHeaderFrame = unpack('!6s6sH', ethernetHeader)

		#converts from network byte order to the host byte order
		ethernetProtocol = ethHeaderFrame[2]

		#ethernetProtocol == the protocol of the packet being carried in the ethernet frame
		#we are only parsing IPv4 packets

		# parse IP packets
		if ethernetProtocol == 2048:

			#IP header is also 20 bytes long
			ipHeader = packet[ethernetHeaderLength:20+ethernetHeaderLength]

			#unpacks into iph array based on the IPv4 header format
			#B = byte, H = 2 bytes, 4s = char array of 4 bytes
			ipHeaderFrame = unpack('!BBHHHBBH4s4s', ipHeader)

			sourceIP = ipHeaderFrame[8]
			destIP = ipHeaderFrame[9]

			sourceIP = socket.inet_ntoa(sourceIP)
			destIP = socket.inet_ntoa(destIP)
			packetString += "IP\nSource IP: {}\nDest IP: {}\n\n".format(sourceIP, destIP)
			#version_ihl = ipHeaderFrame[0]
			#version = version_ihl >> 4

			ihlField = ipHeaderFrame[0] & 0xF
			
			#ihl is the number of 32-bit words in the header, so multiple by 4 to get bytes
			ipHeaderLength = ihlField * 4
	 
			ttl = ipHeaderFrame[5]
			protocol = ipHeaderFrame[6]

			#  parse TCP packets -> must check packets that go to port 80 for HTTP packets
			if protocol == 6 :

				ethAndIPLength = ipHeaderLength + ethernetHeaderLength

				tcpHeader = packet[ethAndIPLength:ethAndIPLength+20]
	 
				#now unpack them :)
				tcpHeaderFrame = unpack('!HHLLBBHHH' , tcpHeader)
				 
				source_port = tcpHeaderFrame[0]
				dest_port = tcpHeaderFrame[1]
				sequence = tcpHeaderFrame[2]
				dataOffset = tcpHeaderFrame[4]

				tcpHeaderLength = dataOffset >> 4
				packetString += "TCP\nSource Port: {}\nDest Port: {}\nSeq #: {}\n\n".format(source_port, dest_port, sequence)
				#print('Source Port : ' + str(source_port) + ' Dest Port : ' + str(dest_port) + ' Sequence Number : ' + str(sequence) + ' Acknowledgement : ' + str(acknowledgement) + ' TCP header length : ' + str(tcpHeaderLength))
				
				ethIPandTCPLength = ethernetHeaderLength + ipHeaderLength + tcpHeaderLength * 4
				data_size = len(packet) - ethIPandTCPLength
				 
				#get data from the packet
				data = packet[ethIPandTCPLength:]
				
				data = data.decode('UTF-8', 'ignore')

				#HTTP Packet
				if (source_port == 80 or dest_port == 80):

					if (len(data) > 0):
						#print("Length of data is {}\n".format(len(data)))
						#print(data)
						#time.sleep(2)
						data.replace('\00','')
						packetString += "HTTP Data: \n"
						packetString += data
						packetString += "\n\n\n\n"
						allPackets.append(packetString)
		
			#UDP Packets -> Must check port 53 for DNS packets
			if protocol == 17:
				ethAndIPLength = ipHeaderLength + ethernetHeaderLength

				#UDP Header is always 8 bytes long
				udpHeaderLength = 8

				udpHeader = packet[ethAndIPLength:ethAndIPLength+8]
	 
				udpHeaderFrame = unpack('!HHHH', udpHeader)
				 
				source_port = udpHeaderFrame[0]
				dest_port = udpHeaderFrame[1]
				udpLength = udpHeaderFrame[2]

				udpDataLength = udpLength - udpHeaderLength - 12

				packetString += "UDP\nSource Port: {}\nDest Port: {}\n\n".format(source_port, dest_port)
				#print('Source Port : ' + str(source_port) + ' Dest Port : ' + str(dest_port) + ' Length : ' + str(length) + ' Checksum : ' + str(checksum))
				 
				ethIPandUDPLength = ethernetHeaderLength + ipHeaderLength + udpHeaderLength

				data_size = len(packet) - ethIPandUDPLength
				 
				#get data starting from first byte after UDP header to end of packet
				data = packet[ethIPandUDPLength:ethIPandUDPLength+12]

				#DNS Packet
				if (source_port == 53 or dest_port == 53):
					#data.replace(b'\00',b'')

					#data = data.decode('utf-8', 'ignore')
					
					if (len(data) > 0):
						#print(data)
						packetString += "DNS Data: \n"
						#packetString += data
						#acketString += "\n\n\n\n"
						#allPackets.append(packetString)

						dnsFrame = unpack("!HHHHHH", data)

						transID = dnsFrame[0]
						flags = dnsFrame[1]
						numQuestions = dnsFrame[2]
						numAnswers = dnsFrame[3]
						numAuthority = dnsFrame[4]
						numAdditional = dnsFrame[5]

						#0x100 -> Standard Query
						if (flags == 256):
							packetString += "Standard Query \n"
							packetString += "Questions: {}\n".format(numQuestions)
							packetString += "Answer RRs: {}\n".format(numAnswers)
							packetString += "Authority RRs: {}\n".format(numAuthority)
							packetString += "Additional RRs: {}\n".format(numAdditional)

							name = packet[ethIPandUDPLength + 12:len(packet) - 3]
							packetString += "Name: {}\n".format(name.decode())
							restofData = packet[-4:]
							lastFrame = unpack("!HH", restofData)
							packetString += "Type: {}\n".format(lastFrame[0])
							packetString += "Class: {}\n".format(lastFrame[1])
							packetString += "\n\n\n\n"
							allPackets.append(packetString)

						if (flags == 33152 or flags == 33155):
							packetString += "Standard Response \n"
							packetString += "Questions: {}\n".format(numQuestions)
							packetString += "Answer RRs: {}\n".format(numAnswers)
							packetString += "Authority RRs: {}\n".format(numAuthority)
							packetString += "Additional RRs: {}\n".format(numAdditional)

							allPackets.append(packetString)


	# Write all packets to the dump file
	output = open(dumpFile,'w')
	output.write(delimiter.join(allPackets))
	output.close()
	rawFile.close()

# Search the dump file for a given regex, print out packet number and packet
def search(regex):
    inputFile = open(dumpFile,'r')
    data = inputFile.read()
    data = data.split(delimiter)
    for i in range(len(data)):
        p = data[i]
        if re.search(regex,p) is not None:
            print("Packet Number: %s\nPacket: \n%s\n" % (i,p))
    inputFile.close()

# Lists all HTTP packets
def httpList():
    inputFile = open(dumpFile,'r')
    data = inputFile.read()
    data = data.split(delimiter)
    httpMethods = ["OPTIONS","GET","HEAD","POST","PUT","DELETE","TRACE","CONNECT"]
    r = "(("+"|".join(httpMethods)+").*)?(HTTP)"
    for i in range(len(data)):
        p = data[i].strip()
        if re.search(r,p) is not None:
            print("Packet Number: %s\nPacket: \n%s\n" % (i,p))

def httpReconstruct(id):
	global httpCount
	id = int(id)
	inputFile = open(dumpFile,'r')
	data = inputFile.read().rstrip()
	data = data.split(delimiter)
	if (id >= len(data) or id < 0):
		print("Invalid packet ID. Type \"http list\" for all http packet numbers.")
		inputFile.close()
		return
	packetToReconstruct = data[id]
	splitPacket = packetToReconstruct.split("<html>")
	
	if len(splitPacket) == 1:
		print("Cannot reconstruct this packet. Enter packet ID with 200 OK Message")
		return
	outputFile = "index" + str(httpCount) + ".html"
	reconstructedHTTP = open(outputFile, 'w')
	reconstructedHTTP.write(splitPacket[len(splitPacket)-1].rstrip())

	if (splitPacket[len(splitPacket)-1].rstrip().endswith("</html>") == False):
		reconstructedHTTP.write("</html>")
	httpCount += 1;
	print("Reconstruction is complete and located in file '"+outputFile+"'.")
